import React, { Component } from 'react';
import {auth, db} from '../firebase';

import { Link } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Signup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : "",
            password : "",
            firstName: "",
            lastName: "",
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        const { email, password } = this.state;
        auth.createUserWithEmailAndPassword(email, password)
        .then(authUser => {
            this.sendVerification();
            db.ref('users/' + authUser.uid).set({firstName: this.state.firstName, lastName: this.state.lastName});
            this.props.history.push('/');
        })
        .catch(authError => {
            alert(authError);
        })
    }
    sendVerification(){

        var user = auth.currentUser;

        user.sendEmailVerification().then(function() {


        }).catch(function(error) {
            // An error happened.
        });

    }


    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    render() {
        const { email, password, firstName, lastName } = this.state;
        const classes = this.props.classes;
        return (
            <div>
                <Grid container>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <h1>Sign up</h1>
                            <form onSubmit={this.onSubmit} autoComplete="off">
                                <TextField
                                    id="firstName"
                                    label="First name"
                                    className={classes.textField}
                                    value={firstName}
                                    onChange={this.handleChange('firstName')}
                                    margin="normal"
                                />
                                <br />
                                <TextField
                                    id="lastName"
                                    label="Last name"
                                    className={classes.textField}
                                    value={lastName}
                                    onChange={this.handleChange('lastName')}
                                    margin="normal"
                                />
                                <br />
                                <TextField
                                  id="email"
                                  label="Email"
                                  className={classes.textField}
                                  value={email}
                                  onChange={this.handleChange('email')}
                                  margin="normal"
                                  type="email"
                                />
                                <br />
                                <TextField
                                  id="password"
                                  label="Password"
                                  className={classes.textField}
                                  value={password}
                                  onChange={this.handleChange('password')}
                                  margin="normal"
                                  type="password"
                                />
                                <br />
                                <Button variant="raised" color="primary" type="submit">Sign up</Button>
                            </form>
                            <p>Have an account? <Link to="/login">Login here</Link></p>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(Signup);
