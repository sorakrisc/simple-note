import React, { Component } from 'react';
import {  Route, withRouter } from 'react-router-dom';
import { auth } from '../firebase';

import './html.css'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';

import PrivateRoute from './PrivateRoute';
import Main from './Main';
import Login from './Login';
import Signup from './Signup';
import Profile from './Profile';
const theme = createMuiTheme();

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            authenticated: false,
            currentUser: null,
            };
        }

    componentWillMount() {
        auth.onAuthStateChanged(user => {

        if (user && user.emailVerified ) {
            if(window.location.pathname==="/login") {
                this.setState({
                        authenticated: true,
                        currentUser: user,
                        loading: false
                    },
                    () => {
                        this.props.history.push('/')
                    }
                );
            }
            else{
                this.setState({
                        authenticated: true,
                        currentUser: user,
                        loading: false
                    },
                );
            }
        }
        else if (user && user.providerData["0"].providerId==="facebook.com"){
            this.setState({
                    authenticated: true,
                    currentUser: user,
                    loading: false
                },
                () => {
                    this.props.history.push('/')
                }
            );
        }
        else {
            this.setState({
                authenticated: false,
                currentUser: null,
                loading: false
            });
        }
        });
    }

    render () {
        const { authenticated, loading } = this.state;
        const content = loading ? (
            <div align="center">
                <CircularProgress size={80} thickness={5} />
            </div>
        ) : (
            <div>
                    <PrivateRoute
                        exact
                        path="/"
                        component={Main}
                        authenticated={authenticated}
                        />
                    <PrivateRoute
                        exact
                        path="/profile"
                        component={Profile}
                        authenticated={authenticated}
                    />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/signup" component={Signup} />
            </div>
        );

        return (
            <MuiThemeProvider theme={theme}>
                <div>
                    <AppBar position="static" color="default">
                        <Toolbar style={{display:'flex', justifyContent:'space-between'}}>
                            <Typography variant="title" color="inherit">
                                Simple Note
                            </Typography>
                            <div>
                                { authenticated && window.location.pathname!=='/profile' &&
                                    <Button variant="raised" color="default" onClick={() => this.props.history.push('/profile')}>Profile</Button>
                                }
                                { authenticated && window.location.pathname==='/profile' &&
                                <Button variant="raised" color="default" onClick={() => window.location.assign('/')}>Home</Button>
                                }
                                { authenticated &&
                                    <Button variant="raised" color="default" onClick={() => auth.signOut()}>Log out</Button>
                                }
                            </div>
                        </Toolbar>
                    </AppBar>
                    { content }
                </div>
            </MuiThemeProvider>
         );
    }
}

export default withRouter(App);
