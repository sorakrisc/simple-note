import React, { Component } from 'react';
import {auth, db} from '../firebase';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import ExpansionPanel, {
    ExpansionPanelSummary,
    ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
        padding: theme.spacing.unit * 2,
        // paddingRight: theme.spacing.unit * 16
    },
    textField: {
        paddingRight: theme.spacing.unit * 2
    },
    expandDetails: {
        // padding: theme.spacing.unit * 4,
        paddingLeft: theme.spacing.unit * 8,
        // display: "flex",
        // justifyContent: "space-between",
        width: "75%",
    },
    button:{
        // padding: theme.spacing.unit *4,
    }
});

class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : auth.currentUser.email,
            emailInput : "",
            password : "",
            passwordInput: "",
            oldPassword : "",
            oldPasswordInput : "",
            confirmPasswordInput: "",
            fName : "",
            lName : "",
            fNameInput : "",
            lNameInput : "",
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        const uid = auth.currentUser.uid;
        // console.log(uid);
        this.updateNameField(uid);

    }
    updateNameField(uid){
        db.ref('users/' + uid).once('value', snapshot => {
            this.setState({
                fName: snapshot.val().firstName,
                lName: snapshot.val().lastName,
                fNameInput: snapshot.val().firstName,
                lNameInput: snapshot.val().lastName,
            });
        })


    }
    changeName(event) {
        const uid = auth.currentUser.uid;
        db.ref('users/'+uid).update({firstName: this.state.fNameInput, lastName: this.state.lNameInput});
        this.updateNameField(uid);
    }

    changeEmail(event) {
        event.preventDefault();
        let newEmail = this.state.emailInput;
        if(newEmail.length >0) {
            // auth.signInWithEmailAndPassword(this.state.email, password)
            auth
                .signInWithEmailAndPassword(this.state.email, this.state.confirmPasswordInput)
                .then(function(user) {
                    user.updateEmail(newEmail)
                        .then(function(user){
                            auth.currentUser.sendEmailVerification()
                            .then(function() {
                                window.location.assign('/login')

                            }).catch(function(error) {
                                // console.log(error)
                            });

                    })
                })
                .catch(authError => {
                    alert(authError);
                })
            // auth.currentUser.updateEmail(this.state.emailInput);
            // console.log(this.state.emailInput);
        }
    }
    changePassword(event){
        event.preventDefault();
        auth.signInWithEmailAndPassword(this.state.email, this.state.oldPasswordInput)

            .then(user => {
                user.updatePassword(this.state.passwordInput).then(function() {
                    // Update successful.
                    alert("Password changed")
                }, function(error) {
                    // An error happened.
                });
            })
            .catch(Error => {
                alert(Error);
            })
    }
    onSubmit(event) {
        event.preventDefault();
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    render() {

        const { email, password, fName, lName, fNameInput, lNameInput } = this.state;
        const classes = this.props.classes;
        return (
            <div>
                <Grid container>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <div className={classes.root}>
                                <ExpansionPanel>
                                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                        <Typography className={classes.heading}>Name</Typography>
                                        <Typography className={classes.heading}>{fName} {lName}</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails className={classes.expandDetails}>
                                        <div>
                                            <TextField
                                                id="fName"
                                                value={fNameInput}
                                                className={classes.textField}
                                                label="First name"
                                                onChange={this.handleChange('fNameInput')}
                                                margin="normal"
                                            />
                                            <TextField
                                                id="lName"
                                                value={lNameInput}
                                                className={classes.textField}
                                                label="Last name"
                                                onChange={this.handleChange('lNameInput')}
                                                margin="normal"
                                            />
                                        </div>
                                        <div style={{paddingTop: "25px"}}>
                                            <Button variant="raised" color="primary" className={classes.button} onClick={(e)=>this.changeName(e)}>
                                                Change
                                            </Button>
                                        </div>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>

                                <ExpansionPanel>
                                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                        <Typography className={classes.heading}>Email</Typography>
                                        <Typography className={classes.heading}>{email}</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails className={classes.expandDetails}>
                                        <div>
                                            <TextField
                                                id="email"
                                                defaultValue={email}
                                                className={classes.textField}
                                                label="New email"
                                                onChange={this.handleChange('emailInput')}
                                                margin="normal"
                                                type="email"
                                            />
                                            <TextField
                                                id="confirmPassword"
                                                className={classes.textField}
                                                label="Current password"
                                                onChange={this.handleChange('confirmPasswordInput')}
                                                margin="normal"
                                                type="password"
                                            />
                                        </div>
                                        <div style={{paddingTop: "25px"}}>
                                            <Button variant="raised" color="primary" className={classes.button} onClick={(e)=>this.changeEmail(e)}>
                                                Change
                                            </Button>
                                        </div>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>

                                <ExpansionPanel>
                                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                        <Typography className={classes.heading}>Password</Typography>
                                        <Typography className={classes.heading} type="password">***********</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails className={classes.expandDetails}>
                                        <div>
                                            <TextField
                                                id="oldPassword"
                                                className={classes.textField}
                                                label="Old password"
                                                onChange={this.handleChange('oldPasswordInput')}
                                                margin="normal"
                                                type="password"
                                            />
                                            <TextField
                                                id="password"
                                                className={classes.textField}
                                                label="New password"
                                                onChange={this.handleChange('passwordInput')}
                                                margin="normal"
                                                type="password"
                                            />
                                        </div>
                                        <div style={{paddingTop: "25px"}}>
                                            <Button onClick={(e)=>this.changePassword(e)} variant="raised" color="primary" className={classes.button}>
                                                Change
                                            </Button>
                                        </div>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </div>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(Profile);
