import React, { Component } from 'react';
import {provider, auth, db} from '../firebase';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import { Icon } from 'react-icons-kit'
import {facebookOfficial} from 'react-icons-kit/fa/facebookOfficial'
import {stickyNoteO} from 'react-icons-kit/fa/stickyNoteO'
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
});
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : "",
            password : ""
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    onSubmit(event) {
        event.preventDefault();
        const { email, password } = this.state;
        auth.signInWithEmailAndPassword(email, password)
        .then(authUser => {
            // console.log(authUser);
            // console.log("hi");
            if(!authUser.emailVerified){
                alert("Please verify your email")
            }
        })
        .catch(authError => {
            alert(authError);
        })
    }
    forgetPass(e){
        auth.sendPasswordResetEmail(this.state.email).then(function() {
            alert("check your email.");
        }).catch(function(error) {
            // An error happened.
        });
    }
    facebookLogin= ()=>{
        auth.signInWithPopup(provider).then(function(result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            // let token = result.credential.accessToken;
            // The signed-in user info.
            let user = result.user;
            // console.log(result);
            let name= user.displayName.split(" ");
            db.ref('users/' + user.uid).set({firstName: name[0] , lastName: name[1]});
        }).catch(function(error) {
            // Handle Errors here.
            // console.log(error);
            // let errorCode = error.code;
            // let errorMessage = error.message;
            // // The email of the user's account used.
            // let email = error.email;
            // // The firebase.auth.AuthCredential type that was used.
            // let credential = error.credential;
            // ...
        });
    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    render() {
        const { email, password } = this.state;
        const classes = this.props.classes;
        return (
            <Grid container>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <div style={{color:"#7ab9eb"}}>
                        <Icon size={64} icon={stickyNoteO} />
                        </div>
                        <form onSubmit={this.onSubmit} autoComplete="off">
                            <TextField
                              id="email"
                              label="Email"
                              className={classes.textField}
                              value={email}
                              onChange={this.handleChange('email')}
                              margin="normal"
                              type="email"
                            />
                            <br />
                            <TextField
                              id="password"
                              label="Password"
                              className={classes.textField}
                              value={password}
                              onChange={this.handleChange('password')}
                              margin="normal"
                              type="password"
                            />
                            <br />
                            <Button variant="raised" color="primary" type="submit">Log in</Button>
                        </form>
                        <br />
                        <Button style={{textTransform:"none"}} variant="raised" onClick={(e)=>this.facebookLogin(e)} color={"primary"}>
                            <Icon style={{paddingRight:5}} icon={facebookOfficial} />
                            Login with Facebook
                        </Button>
                        <br />
                        <Button style={{textTransform:"none"}} onClick={()=>this.props.history.push("/signup")} color={"primary"}>Don't have an account?</Button>
                        <br />
                        <Button style={{textTransform:"none"}} onClick={(e)=>this.forgetPass(e)} color={"primary"}>Forget password?</Button>

                        <div>
                        </div>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(Login);
