import firebase from 'firebase';


const config = {
    apiKey: "AIzaSyBInESPQqJZBTRZ5I6mZyZBRsphnUFl8S8",
    authDomain: "simple-note-49f0f.firebaseapp.com",
    databaseURL: "https://simple-note-49f0f.firebaseio.com",
    projectId: "simple-note-49f0f",
    storageBucket: "",
    messagingSenderId: "257407339077"
};

firebase.initializeApp(config);

export default firebase;
export const db = firebase.database();
export const auth = firebase.auth();
export const provider = new firebase.auth.FacebookAuthProvider();